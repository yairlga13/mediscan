import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton, QVBoxLayout, QFileDialog
from PyQt5.QtGui import QPixmap, QIcon, QFont, QImage
from PyQt5.QtCore import Qt, QTimer,QRect
import cv2
import numpy as np
import easyocr

class MedicamentoDetector(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('MediScan')
        self.setWindowIcon(QIcon("MediScan.jpg"))
        self.setStyleSheet("background-color: #4fc0e8; font-family: Arial;")


        # Define el porcentaje del ancho y el alto deseados para la ventana
        width_percent = 100  # Porcentaje del ancho de la pantalla
        height_percent = 80  # Porcentaje del alto de la pantalla

        # Calcula el tamaño relativo en función de los porcentajes
        screen_geometry = QApplication.desktop().availableGeometry()
        width = screen_geometry.width() * width_percent // 100
        height = screen_geometry.height() * height_percent // 100

        # Calcula la posición de la ventana para que esté centrada en la pantalla
        x = (screen_geometry.width() - width) // 2
        y = (screen_geometry.height() - height) // 2

        # Establece las dimensiones y la posición de la ventana
        self.setGeometry(x, y, width, height)

        # Add logo and title in the header
        self.logo_label = QLabel()
        self.logo_pixmap = QPixmap('MediScan.jpg')
        self.logo_label.setPixmap(self.logo_pixmap.scaledToWidth(500))
        self.header_layout = QVBoxLayout()
        self.header_layout.addWidget(self.logo_label, alignment=Qt.AlignCenter)


        self.label_nombre = QLabel('Nombre del medicamento:')
        self.label_nombre.setStyleSheet("font-size: 20px; color: #04377e; font-weight: bold;")
        self.label_descripcion = QLabel('Descripción:')
        self.label_descripcion.setStyleSheet("font-size: 18px; color: #000;")
        self.label_descripcion.setWordWrap(True)  # Enable word wrap for description
        self.label_imagen = QLabel('Imagen:')
        self.label_imagen.setAlignment(Qt.AlignCenter)

        self.button = QPushButton('Capturar Imagen')
        self.button.setStyleSheet("background-color: #04377e; color: #fff; font-size: 18px; padding: 10px;")
        self.button.clicked.connect(self.capturar_imagen)

        layout = QVBoxLayout()
        layout.addLayout(self.header_layout)  # Add header layout
        layout.addWidget(self.label_nombre)
        layout.addWidget(self.label_descripcion)
        layout.addWidget(self.label_imagen)
        layout.addWidget(self.button)
        self.setLayout(layout)

        # Initialize OCR reader
        self.lector_ocr = easyocr.Reader(['en'])

        # Start camera capture
        self.cap = cv2.VideoCapture(0)  # Use the default camera (index 0)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.actualizar_camara)
        self.timer.start(100)  # Update every 100 milliseconds

        # Connect the application closing event to the cleanup method
        app.aboutToQuit.connect(self.liberar_recursos)

        # Diccionario de medicamentos con descripciones
        self.medicamentos = {
            'paracetamol': {
                'descripcion': 'El paracetamol es un analgésico y antipirético ampliamente utilizado para el alivio del dolor y la fiebre.'
            },
            'naproxeno': {
                'descripcion': 'El naproxeno es un antiinflamatorio no esteroideo (AINE) que se utiliza para el tratamiento del dolor y la inflamación.'
            },
            'omeprazol': {
                'descripcion': 'El omeprazol es un medicamento que pertenece a la clase de los inhibidores de la bomba de protones (IBP). Se utiliza principalmente para tratar afecciones relacionadas con el ácido estomacal, como la enfermedad por reflujo gastroesofágico (ERGE), úlceras pépticas, esofagitis por reflujo y otras condiciones en las que se produce un exceso de ácido en el estómago.'
            },
            'metformina': {
                'descripcion': 'La metformina es un medicamento antidiabético oral utilizado para el control de la glucosa en la sangre en pacientes con diabetes tipo 2.'
            },
            'amoxicilina': {
                'descripcion': 'La amoxicilina es un antibiótico de amplio espectro que se utiliza para tratar infecciones bacterianas en diversas partes del cuerpo, como infecciones del tracto respiratorio, infecciones de la piel, infecciones del oído y otras afecciones.'
            },
            'simvastatina': {
                'descripcion': 'La simvastatina es un medicamento utilizado para reducir los niveles de colesterol en sangre. Pertenece a la clase de medicamentos llamados estatinas.'
            },
            'loratadina': {
                'descripcion': 'La loratadina es un antihistamínico que se utiliza para aliviar los síntomas de la alergia, como la picazón, la secreción nasal y los ojos llorosos.'
            },
            'ciprofloxacino': {
                'descripcion': 'El ciprofloxacino es un antibiótico que se utiliza para tratar infecciones bacterianas, como infecciones del tracto urinario, infecciones respiratorias, infecciones de la piel y tejidos blandos, entre otras.'
            },
            'dexametasona': {
                'descripcion': 'La dexametasona es un glucocorticoide que se utiliza para tratar una variedad de afecciones, como inflamaciones, alergias, enfermedades autoinmunes y algunas condiciones respiratorias.'
            },
            'fluconazol': {
                'descripcion': 'El fluconazol es un antifúngico que se utiliza para tratar infecciones fúngicas, como la candidiasis vaginal, la candidiasis oral, la criptococosis y otras infecciones por hongos.'
            },
            'metoprolol': {
                'descripcion': 'El metoprolol es un betabloqueante utilizado para tratar afecciones como la hipertensión, la angina de pecho y algunos trastornos del ritmo cardíaco.'
            },
            'ranitidina': {
                'descripcion': 'La ranitidina es un antagonista de los receptores H2 que se utiliza para reducir la producción de ácido estomacal y tratar afecciones como la acidez estomacal, la úlcera gástrica y el reflujo gastroesofágico.'
            },
            'enalapril': {
                'descripcion': 'El enalapril es un inhibidor de la enzima convertidora de angiotensina (IECA) utilizado para tratar la hipertensión arterial, la insuficiencia cardíaca y otras condiciones cardíacas.'
            },
            'tramadol': {
                'descripcion': 'El tramadol es un analgésico opioidérgico utilizado para el alivio del dolor moderado a severo. Actúa en el sistema nervioso central para bloquear la transmisión del dolor.'
            },
            'sertralina': {
                'descripcion': 'La sertralina es un antidepresivo de la clase de inhibidores selectivos de la recaptación de serotonina (ISRS) utilizado para tratar la depresión, el trastorno obsesivo-compulsivo, el trastorno de pánico, el trastorno de ansiedad social y otros trastornos psiquiátricos.'
            },
            'diclofenaco': {
                'descripcion': 'El diclofenaco es un antiinflamatorio no esteroideo (AINE) que se utiliza para tratar el dolor, la inflamación y la fiebre asociados con diversas condiciones como artritis, lesiones musculoesqueléticas y otros trastornos.'
            },
            'furosemida': {
                'descripcion': 'La furosemida es un diurético de asa utilizado para tratar la retención de líquidos (edema) asociada con afecciones como insuficiencia cardíaca, enfermedad renal y cirrosis hepática.'
            },
            'rosuvastatina': {
                'descripcion': 'La rosuvastatina es un medicamento utilizado para reducir los niveles de colesterol en sangre. Pertenece a la clase de medicamentos llamados estatinas.'
            },
            'valsartan': {
                'descripcion': 'El valsartan es un antagonista de los receptores de angiotensina II utilizado para tratar la hipertensión arterial, la insuficiencia cardíaca y otras condiciones cardíacas.'
            },
            'insulina': {
                'descripcion': 'La insulina es una hormona utilizada para el tratamiento de la diabetes mellitus. Ayuda a regular los niveles de glucosa en sangre al facilitar la absorción de glucosa por parte de las células.'
            },
            'warfarina': {
                'descripcion': 'La warfarina es un anticoagulante utilizado para prevenir la formación de coágulos sanguíneos. Se usa en el tratamiento y prevención de enfermedades tromboembólicas.'
            },
            'amiodarona': {
                'descripcion': 'La amiodarona es un antiarrítmico utilizado para tratar trastornos del ritmo cardíaco como la fibrilación auricular, la taquicardia ventricular y otras arritmias graves.'
            },
            'budesonida': {
                'descripcion': 'La budesonida es un corticosteroide utilizado para tratar afecciones inflamatorias como el asma, la enfermedad pulmonar obstructiva crónica (EPOC) y otras enfermedades respiratorias.'
            },
            'paroxetina': {
                'descripcion': 'La paroxetina es un antidepresivo de la clase de inhibidores selectivos de la recaptación de serotonina (ISRS) utilizado para tratar la depresión, el trastorno obsesivo-compulsivo, el trastorno de pánico, el trastorno de ansiedad social y otros trastornos psiquiátricos.'
            },
            'duloxetina': {
                'descripcion': 'La duloxetina es un antidepresivo utilizado para tratar la depresión, el trastorno de ansiedad generalizada, la neuropatía diabética y el trastorno de estrés postraumático.'
            },
            'esomeprazol': {
                'descripcion': 'El esomeprazol es un medicamento que pertenece a la clase de los inhibidores de la bomba de protones (IBP). Se utiliza para tratar afecciones relacionadas con el ácido estomacal, como la enfermedad por reflujo gastroesofágico (ERGE), úlceras pépticas, esofagitis por reflujo y otras condiciones en las que se produce un exceso de ácido en el estómago.'
            },
            'pioglitazona': {
                'descripcion': 'La pioglitazona es un medicamento antidiabético utilizado para mejorar el control de la glucosa en sangre en pacientes con diabetes tipo 2.'
            },
            'levotiroxina': {
                'descripcion': 'La levotiroxina es una hormona tiroidea utilizada para tratar el hipotiroidismo, una condición en la que la glándula tiroides no produce suficiente hormona tiroidea.'
            },
            'fenitoína': {
                'descripcion': 'La fenitoína es un anticonvulsivo utilizado para prevenir y controlar las convulsiones en pacientes con epilepsia y otras condiciones que causan convulsiones.'
            },
            'clopidogrel': {
                'descripcion': 'El clopidogrel es un medicamento antiplaquetario utilizado para prevenir la formación de coágulos sanguíneos y reducir el riesgo de ataques cardíacos y accidentes cerebrovasculares.'
            },
            'losartan': {
                'descripcion': 'El losartan es un antagonista de los receptores de angiotensina II utilizado para tratar la hipertensión arterial y la insuficiencia cardíaca.'
            },
            'metilfenidato': {
                'descripcion': 'El metilfenidato es un estimulante del sistema nervioso central utilizado para tratar el trastorno por déficit de atención e hiperactividad (TDAH) en niños y adultos.'
            },
            'nifedipino': {
                'descripcion': 'El nifedipino es un bloqueador de los canales de calcio utilizado para tratar la hipertensión arterial y la angina de pecho.'
            },
            'neomicina': {
                'descripcion': 'La neomicina es un antibiótico aminoglucósido utilizado para tratar infecciones bacterianas en diversas partes del cuerpo, como infecciones de la piel, infecciones del tracto gastrointestinal y otras afecciones.'
            },
            'oxacilina': {
                'descripcion': 'La oxacilina es un antibiótico betalactámico de la clase de las penicilinas utilizadas para tratar infecciones causadas por bacterias grampositivas, como infecciones estafilocócicas.'
            },
            'rifampicina': {
                'descripcion': 'La rifampicina es un antibiótico utilizado para tratar infecciones bacterianas, como la tuberculosis y algunas infecciones por estafilococos y estreptococos.'
            },
            'fluoxetina': {
                'descripcion': 'La fluoxetina es un antidepresivo de la clase de inhibidores selectivos de la recaptación de serotonina (ISRS) utilizado para tratar la depresión, el trastorno obsesivo-compulsivo, el trastorno de pánico y otros trastornos psiquiátricos.'
            },
            'sildenafil': {
                'descripcion': 'El sildenafil es un medicamento utilizado para tratar la disfunción eréctil en hombres. También se utiliza en el tratamiento de la hipertensión arterial pulmonar.'
            },
            'tadalafil': {
                'descripcion': 'El tadalafil es un medicamento utilizado para tratar la disfunción eréctil en hombres y los síntomas de la hiperplasia prostática benigna (HPB). También se utiliza en el tratamiento de la hipertensión arterial pulmonar.'
            },
            'vardenafil': {
                'descripcion': 'El vardenafil es un medicamento utilizado para tratar la disfunción eréctil en hombres.'
            },
            'clotrimazol': {
                'descripcion': 'El clotrimazol es un antifúngico utilizado para tratar infecciones fúngicas de la piel, las uñas y las membranas mucosas, como la candidiasis y la tiña.'
            },
            'salbutamol': {
                'descripcion': 'El salbutamol es un broncodilatador utilizado para aliviar el broncoespasmo en afecciones como el asma y la enfermedad pulmonar obstructiva crónica (EPOC).'
            },
            'heparina': {
                'descripcion': 'La heparina es un anticoagulante utilizado para prevenir la formación de coágulos sanguíneos y tratar afecciones tromboembólicas.'
            },
            'metamizol': {
                'descripcion': 'El metamizol es un analgésico y antipirético utilizado para aliviar el dolor y la fiebre. También se conoce como dipirona.'
            },
            'ambroxol': {
                'descripcion': 'El ambroxol es un mucolítico utilizado para tratar afecciones respiratorias, como la bronquitis, la fibrosis quística y otras enfermedades que causan la acumulación de mucosidad.'
            },
            'guayacolato': {
                'descripcion': 'El guayacolato es un expectorante utilizado para facilitar la expulsión de mucosidad en afecciones respiratorias.'
            },
            'clavulanato': {
                'descripcion': 'El clavulanato es un inhibidor de las betalactamasas utilizado en combinación con ciertos antibióticos para mejorar su eficacia contra bacterias resistentes a los antibióticos.'
            },
            'meloxicam': {
                'descripcion': 'El meloxicam es un antiinflamatorio no esteroideo (AINE) utilizado para tratar el dolor y la inflamación asociados con afecciones como la artritis y la osteoartritis.'
            },
            'prednisona': {
                'descripcion': 'La prednisona es un corticosteroide utilizado para tratar afecciones inflamatorias, alérgicas e inmunológicas, como la artritis, el asma, las reacciones alérgicas y algunas enfermedades autoinmunes.'
            },
            'fenobarbital': {
                'descripcion': 'El fenobarbital es un barbitúrico utilizado como anticonvulsivo para prevenir y controlar las convulsiones en pacientes con epilepsia.'
            },
            'cidofovir': {
                'descripcion': 'El cidofovir es un antiviral utilizado para tratar infecciones causadas por virus, como el citomegalovirus (CMV) y el virus del papiloma humano (VPH).'
            },
            'bimatoprost': {
                'descripcion': 'El bimatoprost es un medicamento utilizado para reducir la presión intraocular en pacientes con glaucoma de ángulo abierto y para mejorar la apariencia de las pestañas en el tratamiento de la hipotricosis.'
            },
            'lactulosa': {
                'descripcion': 'La lactulosa es un laxante utilizado para tratar el estreñimiento y reducir la acumulación de amoníaco en pacientes con encefalopatía hepática.'
            },
            'fluticasona': {
                'descripcion': 'La fluticasona es un corticosteroide inhalado utilizado para el tratamiento del asma y la enfermedad pulmonar obstructiva crónica (EPOC). También se utiliza en el tratamiento de la rinitis alérgica.'
            },
            'betametasona': {
                'descripcion': 'La betametasona es un corticosteroide utilizado para tratar afecciones inflamatorias de la piel, las articulaciones, los ojos y otras partes del cuerpo.'
            },
            'amitriptilina': {
                'descripcion': 'La amitriptilina es un antidepresivo tricíclico utilizado para tratar la depresión, el trastorno de ansiedad, el trastorno obsesivo-compulsivo (TOC) y la neuralgia posherpética, entre otras afecciones.'
            },
            'nortriptilina': {
                'descripcion': 'La nortriptilina es un antidepresivo tricíclico utilizado para tratar la depresión y el trastorno de ansiedad.'
            },
            'oxitocina': {
                'descripcion': 'La oxitocina es una hormona utilizada para inducir el parto, estimular la contracción uterina durante el parto y controlar el sangrado posparto.'
            },
            'naloxona': {
                'descripcion': 'La naloxona es un antagonista de los receptores de opioides utilizado para revertir los efectos de los opioides en casos de sobredosis.'
            },
            'bromuro de ipratropio': {
                'descripcion': 'El bromuro de ipratropio es un broncodilatador utilizado para aliviar el broncoespasmo en afecciones como el asma y la enfermedad pulmonar obstructiva crónica (EPOC).'
            },
            'fenilpropanolamina': {
                'descripcion': 'La fenilpropanolamina es un descongestionante utilizado para aliviar la congestión nasal y los síntomas del resfriado y la gripe.'
            },
            'diuréticos': {
                'descripcion': 'Los diuréticos son medicamentos utilizados para aumentar la producción de orina y reducir la retención de líquidos en el cuerpo. Se utilizan en el tratamiento de la hipertensión, la insuficiencia cardíaca y otras afecciones.'
            },
            'antidiabéticos': {
                'descripcion': 'Los antidiabéticos son medicamentos utilizados para controlar los niveles de glucosa en sangre en pacientes con diabetes. Incluyen medicamentos orales, insulina y otros tratamientos.'
            },
            'antihipertensivos': {
                'descripcion': 'Los antihipertensivos son medicamentos utilizados para reducir la presión arterial alta. Incluyen varios tipos de medicamentos, como diuréticos, inhibidores de la enzima convertidora de angiotensina (IECA), bloqueadores de los receptores de angiotensina II (ARA-II), betabloqueantes y otros.'
            },
            'antidepresivos': {
                'descripcion': 'Los antidepresivos son medicamentos utilizados para tratar la depresión, el trastorno de ansiedad, el trastorno obsesivo-compulsivo (TOC), el trastorno de pánico y otras afecciones psiquiátricas. Incluyen varios tipos de medicamentos, como inhibidores selectivos de la recaptación de serotonina (ISRS), inhibidores de la recaptación de serotonina-norepinefrina (IRSN), antidepresivos tricíclicos y otros.'
            },
            'antibióticos': {
                'descripcion': 'Los antibióticos son medicamentos utilizados para tratar infecciones bacterianas. Incluyen varios tipos de medicamentos, como penicilinas, cefalosporinas, macrólidos, quinolonas, tetraciclinas y otros.'
            },
            'analgésicos': {
                'descripcion': 'Los analgésicos son medicamentos utilizados para aliviar el dolor. Incluyen analgésicos de venta libre como el paracetamol y la aspirina, así como analgésicos opioides y otros.'
            },
            'antinflamatorios': {
                'descripcion': 'Los antiinflamatorios son medicamentos utilizados para reducir la inflamación y aliviar el dolor y la fiebre. Incluyen antiinflamatorios no esteroideos (AINE) como el ibuprofeno y el naproxeno, así como corticosteroides y otros.'
            },
            'antihistamínicos': {
                'descripcion': 'Los antihistamínicos son medicamentos utilizados para aliviar los síntomas de la alergia, como la picazón, la secreción nasal y los ojos llorosos. Incluyen antihistamínicos de primera generación y antihistamínicos de segunda generación.'
            }
        }


    def capturar_imagen(self):
        ret, frame = self.cap.read()  # Read a frame from the camera
        if ret:
            cv2.imwrite('captura.jpg', frame)  # Save the captured frame as an image
            self.detectar_medicamento('captura.jpg')

    def detectar_medicamento(self, filename):
        imagen = cv2.imread(filename)
        if imagen is None:
            self.label_nombre.setText('Error al cargar la imagen')
            return

        gray = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
        resultados = self.lector_ocr.readtext(gray)

        medicamento_detectado = False
        for resultado in resultados:
            texto = resultado[1].lower()
            if texto in self.medicamentos:
                medicamento_detectado = True
                nombre_medicamento = texto.capitalize()
                descripcion_medicamento = self.medicamentos[texto]['descripcion']

                self.label_nombre.setText(f'Nombre del medicamento: {nombre_medicamento}')
                self.label_descripcion.setText(f'Descripción: {descripcion_medicamento}')

                pixmap = QPixmap(filename)
                self.label_imagen.setPixmap(pixmap.scaledToWidth(400))

                break

        if not medicamento_detectado:
            self.label_nombre.setText('Medicamento desconocido')
            self.label_descripcion.clear()
            self.label_imagen.clear()

    def actualizar_camara(self):
        ret, frame = self.cap.read()  # Read a frame from the camera
        if ret:
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # Convert BGR to RGB format
            height, width, channel = frame.shape
            bytes_per_line = channel * width
            image = QImage(frame_rgb.data, width, height, bytes_per_line, QImage.Format_RGB888)
            pixmap = QPixmap.fromImage(image)
            self.label_imagen.setPixmap(pixmap.scaledToWidth(400))

    def liberar_recursos(self):
        self.cap.release()

# Start the application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ventana = MedicamentoDetector()
    ventana.show()
    sys.exit(app.exec_())